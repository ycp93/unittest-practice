#!/usr/bin/env python3

from setuptools import setup, find_packages

setup(
    name="unittest_practice",
    version="0.0.1",
    install_requires=['biopython'],
    author="Young-Chan Park",
    author_email="young.chan.park93@gmail.com")
