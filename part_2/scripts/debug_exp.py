#!/usr/bin/env python3

from Bio.SeqUtils import IUPACData

def print_dict():
    print(IUPACData.protein_letters_1to3)


def change_1_to_3(string):
    new_list = list()
    for i in list(string):
        if i.isupper():
            new_list.append(IUPACData.protein_letters_1to3[i])
        else:
            new_list.append(i)
    return ''.join(new_list)



def change_all_1to3(file_path):
    with open(file_path, mode = 'r') as f:
        aa_list = f.read().split('\n')
        for aa in aa_list:
            print(aa, change_1_to_3(aa))
