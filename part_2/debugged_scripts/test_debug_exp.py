import unittest
import debug_exp

class TestMain(unittest.TestCase):

    def test_1to3(self):
        output = debug_exp.change_1_to_3('V600E')
        expected = 'Val600Glu'

        self.assertEqual(output, expected)

    def test_1to3_when_no_change_to_make(self):
        output = debug_exp.change_1_to_3('963_1010del')
        expected = '963_1010del'

        self.assertEqual(output, expected)
    
    def test_1to3_XtoTer(self):
        output = debug_exp.change_1_to_3('R805X')
        expected = 'Arg805Ter'
        
        self.assertEqual(output, expected)

if __name__ == '__main__':
    unittest.main()
