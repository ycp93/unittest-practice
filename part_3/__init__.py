#!/usr/bin/env python3

from Bio.SeqUtils import IUPACData


class Toolkit:
    
    one_to_three = IUPACData.protein_letters_1to3.copy()
    one_to_three['X'] = 'Ter'
    one_to_three['*'] = 'Ter'

    def __init__(self, sanger_cutoff = 33):
        self.sanger_cutoff = sanger_cutoff

    def change_1_to_3(self, string):
        new_list = list()
        for i in list(string):
            if (i.isupper()) or (i == '*'): # "or (i == '*')" added
                new_list.append(self.one_to_three[i])
            else:
                new_list.append(i)
        return ''.join(new_list)

    def get_qual_score(self, ascii_char: str):
        return ord(ascii_char) - self.sanger_cutoff
