#!/usr/bin/env python3
# All credits to Corey Schafer (I love you Corey Schafer!)
# https://github.com/CoreyMSchafer/code_snippets/blob/master/Python-Unit-Testing/test_calc.py

import unittest
import calc


class TestCalc(unittest.TestCase):

    def test_add(self):
        self.assertEqual(calc.add(10, 5), 100000000)
        self.assertEqual(calc.add(-1, -1), -50000000)

# Both asserts fail, but the test will stop after detecting the first assert.

if __name__ == '__main__':
    unittest.main()
