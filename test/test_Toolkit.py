#!/usr/bin/env python3
import sys
import os
import unittest

if __name__ == '__main__':
    sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..'))
    from part_3 import Toolkit

else:
    from part_3 import Toolkit


class TestToolkit(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        cls.toolkit = Toolkit(sanger_cutoff = 33)
        print('Set up!')

    @classmethod
    def tearDownClass(cls):
        print('\nTear down!')

    def test_1to3(self):
        output = self.toolkit.change_1_to_3('V600E')
        expected = 'Val600Glu'
        print('test 1')
        self.assertEqual(output, expected)

    def test_1to3_when_no_change_to_make(self):
        output = self.toolkit.change_1_to_3('963_1010del')
        expected = '963_1010del'
        print('\ntest 2')
        self.assertEqual(output, expected)

    def test_1to3_XtoTer(self):
        output = self.toolkit.change_1_to_3('R805X')
        expected = 'Arg805Ter'
        print('\ntest 3')
        self.assertEqual(output, expected)

    def test_get_qual_score(self):
        output = self.toolkit.get_qual_score('?')
        print('\ntest 4')
        self.assertEqual(output, 30)

if __name__ == '__main__':
    unittest.main()
